// pages/spy/spy.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    dataArr: [{ data: ['太监', '东方不败'] }, { data: ['iphone', 'ipad'] }, { data: ['七喜', '雪碧'] }, { data: ['两小无猜', '青梅竹马'] }, { data: ['玉米', '高粱'] }, { data: ['铅球', '铁饼'] }, { data: ['盗墓笔记', '鬼吹灯'] }, { data: ['梁山伯与祝英台', '罗密欧与朱丽叶'] }, { data: ['关羽', '张飞'] }, { data: ['乔丹', '奥尼尔'] }, { data: ['中国移动', '中国联通'] }, { data: ['吸血鬼', '僵尸'] }, { data: ['火车', '地铁'] }, { data: ['诺基亚', '三星'] }, { data: ['好好学习', '天天向上'] }, { data: ['散文', '小说'] }, { data: ['移花接木', '斗转星移'] }, { data: ['空气', '氧气'] }, { data: ['淘宝', '京东'] }, { data: ['抖音', '快手'] }, { data: ['微博', '微信'] }, { data: ['北京爱情故事', '乡村爱情故事'] }, { data: ['橙子', '橘子'] }, { data: ['香港', '澳门'] }, { data: ['剩女', '御姐'] }, { data: ['保安', '保镖'] }, { data: ['金庸', '古龙'] }, { data: ['人妖', '太监'] }, { data: ['面包', '蛋糕'] }, { data: ['魔术师', '魔法师'] }, { data: ['蜘蛛侠', '蝙蝠侠'] }, { data: ['富二代', '高富帅'] }, { data: ['泡泡糖', '棒棒糖'] }, { data: ['小沈阳', '宋小宝'] }, { data: ['牛肉干', '猪肉铺'] }, { data: ['包青天', '狄仁杰'] }, { data: ['丑小鸭', '灰姑娘'] }, { data: ['福尔摩斯', '工藤新一'] }, { data: ['降龙十八掌', '九阴白骨爪'] }, { data: ['鸡蛋', '鸭蛋'] }, { data: ['红烧牛肉面', '香辣牛肉面'] }, { data: ['鼠目寸光', '井底之蛙'] }, { data: ['过山车', '碰碰车'] }, { data: ['洗发露', '护发素'] }, { data: ['果粒橙', '鲜橙多'] }, { data: ['土豆粉', '酸辣粉'] }, { data: ['小笼包', '灌汤包'] }],
    index: 1,
    memberNum: 10,
    // 总卧底数量
    AmemberNum: 1,
    openWhiteboard: false,
    oldArr: ['太监', '东方不败'],
    nowArr: ['太监', '太监', '太监', '太监','东方不败'],
    classArr: [1,1,1,1,1],
    // 牌面正反
    lookStatus: false,
    // 牌内容
    lookContent: '',
    // 是否已查看
    looked: false,
    buttonTitle: '下一位',

    // 看牌结束，卧底开始标志
    flag: false,
    yesNum: 0,
    noNum: 0,
    resultShow: 0,// 0代表游戏继续，1代表卧底胜利，2代表平民胜利,
  },

  _look: function() {
    if (!this.data.looked || this.data.lookStatus) {
      let lookStatus = !this.data.lookStatus;
      this.setData({
        lookStatus: lookStatus,
        looked: true
      })
    }
  },

  _next: function() {
    if (this.data.looked) {
      let num = this.data.nowArr.length;
      let i = this.data.index + 1;
      let content = this.data.nowArr[i - 1];
      if (this.data.index < num) {
        if (i == num) {
          this.setData({
            buttonTitle: '卧底开始'
          })
        }
        this.setData({
          lookStatus: false,
          looked: false,
          index: i,
          lookContent: content
        })
      } else {
        // 卧底开始
        this.setData({
          flag: true
        })
      }
    }

  },

  // 换一组数据
  _change: function() {
    this._putArr();
    this.setData({
      lookContent: this.data.nowArr[0],
      lookStatus: false,
      looked: false,
      index: 1,
    })

  },

  // 获取某个范围内随机整数
  rand: function(n, m) {
    let c = m - n + 1;
    return Math.floor(Math.random() * c + n);
  },

  // 通过dataArr生成nowArr
  _putArr: function() {
    let data = this.data.dataArr;
    let result = new Array();
    for (let i = 0; i < 1; i++) {
      let ran = Math.floor(Math.random() * data.length);
      result.push(data.splice(ran, 1)[0]);
    };
    let Arr = result[0].data;
    this.setData({
      oldArr: Arr
    })
    let newArr = new Array();
    for (let i = 0; i < (this.data.memberNum-this.data.AmemberNum); i++) {
      newArr.push(Arr[0]);
    }
    for (let i = 0; i < this.data.AmemberNum;i++){
      let num = this.rand(1, this.data.memberNum);
      newArr.splice(num, 0, Arr[1]);
    }
    this.setData({
      nowArr: newArr
    })
    console.log(this.data.oldArr);
    console.log(this.data.nowArr);
    console.log(this.data.nowArr[0]);

    let classArr = new Array();
    for (let i = 0; i < this.data.memberNum; i++) {
      classArr.push(1);
    }
    this.setData({
      classArr: classArr
    })
  },

  _see: function(e) {
    let yesNum = this.data.yesNum;
    let noNum = this.data.noNum; 
    if (yesNum > noNum && noNum !== 0) {
      // 游戏进行
      let index = e.currentTarget.dataset.index;
      console.log(index);
      let item = e.currentTarget.dataset.item;
      let classArr = this.data.classArr;
      let oldArr = this.data.oldArr;
      if (item==oldArr[0]) {
        // 平民
        let t = yesNum - 1;
        this.setData({
          yesNum: t
        })
      } else {
        // 卧底
        let t = noNum - 1;
        this.setData({
          noNum: t
        })
      }
      classArr[index] = 0;
      this.setData({
        classArr: classArr
      })
    }
    let numA = this.data.yesNum;
    let numB = this.data.noNum;
    if (numA <= numB) {
      // 游戏结束 卧底胜利
      let that = this;
      setTimeout(function () {
        that.setData({
          resultShow: 1
        })
      }, 600)
      console.log('卧底胜利');
    } else if (numB == 0) {
      // 游戏结束 平民胜利
      let that = this;
      setTimeout(function () {
        that.setData({
          resultShow: 2
        })
      }, 600)
      console.log('平民胜利');
    }

    console.log(this.data.resultShow);
  },


  _redirect: function(){
    wx.reLaunch({
      url: './spy'
    });
  },

  _goto: function() {
    wx.redirectTo({
      url: '../game1/game1'
    });
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let memberNum = wx.getStorageSync('memberNum');
    let AmemberNum = wx.getStorageSync('AmemberNum');
    let openWhiteboard = wx.getStorageSync('openWhiteboard');
    this.setData({
      memberNum: memberNum,
      AmemberNum: AmemberNum,
      openWhiteboard: openWhiteboard,
      yesNum: memberNum - AmemberNum,
      noNum: parseInt(AmemberNum)
    })
    this._putArr();
    this.setData({
      lookContent: this.data.nowArr[0]
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})
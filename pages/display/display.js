// pages/display/display.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 所有歌曲
    musicArr: [{
      audioPoster: 'https://y.gtimg.cn/music/photo_new/T001R300x300M000004NfJdF0gn6Z1.jpg?max_age=2592000',
      audioName: 'Red Velvet',
      audioAuthor: 'Russian Roulette',
      audioSrc: 'http://up.mcyt.net/down/45896.mp3',
    }, {
        audioPoster: 'https://y.gtimg.cn/music/photo_new/T001R300x300M0000007y4gL465VWJ.jpg?max_age=2592000',
      audioName: 'Bullet Train',
      audioAuthor: 'Stephen Swartz',
      audioSrc: 'http://up.mcyt.net/down/45892.mp3',
      }, {
        audioPoster: 'https://y.gtimg.cn/music/photo_new/T002R300x300M000002T6LhT4KKxPc.jpg?max_age=2592000',
        audioName: 'M2M',
        audioAuthor: 'Pretty Boy',
        audioSrc: 'http://up.mcyt.net/down/45849.mp3',
      }],
    // 选中的序号
    musicindex: 0,  
    // 选中歌曲  
    dataJson: {
      audioPoster: 'https://y.gtimg.cn/music/photo_new/T001R300x300M000004NfJdF0gn6Z1.jpg?max_age=2592000',
      audioName: 'Red Velvet',
      audioAuthor: 'Russian Roulette',
      audioSrc: 'http://up.mcyt.net/down/45896.mp3',
    },
    gameindex: 0,
    data: '',
    // 真心话
    data0: ['如果看到自己最爱的人熟睡在你面前你会做什么？', '当你最不知道穿什么颜色的时候，你会选择什么颜色？', '最后一次发自内心的笑是什么时候？', '曾经有过最被感动的事是什么？', '说出你最想要的5样东西', '如果给你一个机会去世界上任何一个地方旅行你最想去哪？', '如果时间能倒流你希望回到哪个时间? ', '如果让你拥有一种超能力，你愿意拥有什么呢？', '最喜欢哪部电影？', '最喜欢的食物是什么？', '如果你爱的人不爱你怎么办？', '如果让你选择做一个电影中的角色，你会选谁呢？', '你会选择你爱的人还是爱你的人？', '如果能预知未来，你最不希望看见的是什么？', '现在你最喜欢的人是谁？', '最喜欢看什么动画片？', '在爱情和面包中二选一，你会选择哪个?为什么？', '目前为止你做过最疯狂的事是什么？', '你的初吻是几岁在什么地方被什么人夺去的？', '你的初恋是几岁？', '第一个喜欢的异性叫什么名字？', '对梦中情人有什么要求(在一分钟内说出五条)', '到目前为止一共谈过几次恋爱？', '你在意你的老婆(老公)不是处女(处男)吗？', '结婚后希望生男孩还是女孩(只能选择一个，说出原因)', '说说你作弊使用过的最高招', '如果明天是世界末日，你现在最想做的是什么？', '最欣赏自己哪个部位？', '最不满意自己哪个部位？', '说出内衣颜色', '如果跟你喜欢的人约会，碰到前任的男(女)朋友，会有什么表现？', '无聊的时候一般做什么？', '你身上有没有胎记？长在什么地方，什么形状？', '在和男、女朋友交往的过程中，有被甩过吗？', '请讲述第一次恋爱的情景', '你认为男人善变还是女人善变？', '只给你一天时间当异性，你最想做什么？', '有对朋友撒过谎吗？', '你觉得男女朋友分手后还能做普通朋友吗？', '如果有喜欢的人，你会不会ta表白？', '你认为最浪漫的事情是什么？', '你会接受平胸的吗？', '喜欢男女生做些什么小动作？', '你听过或说过的最感人的情话是什么？', '思想出轨和身体出轨，哪个最不能接受？', '你觉得什么样算是分手最佳方式', '如果一天之内要用光十万元，你要怎么样花？', '你最讨厌什么样的人？', '你接受姐弟恋吗？', '到目前为止写过多少封情书？', '用四个字形容你现在的生活状态？', '你觉得世界上最大的悲剧是什么？', '最长一次连续睡着了多久？', '你最喜欢的小说是什么？', '晚上睡觉要上几次厕所？', '你觉得自己最郁闷的外号是什么？', '你认为在座谁最性感？', '当过第三者么？', '最奢侈的一次消费是什么？', '去你喜欢的人家里想拉肚子怎么办？', '有玩过制服play吗？', '有什么不吃的食物吗？'],
    // 大冒险
    data1: ['当众讲一个你记得（或者现搜）的黄色笑话', '大喊两声“我是禽兽”', '随便找个人喝酒，并说“我是猪”', '随便找个异性喝酒，并说“我关注你好久了，能请喝一杯吗”', '随便找个人喝酒，并跟ta玩个小游戏', '解开你的鞋带然后大喊“啊，我鞋带掉了”', '随便给手机里的一个异性打电话。说：其实.....我是....猪', '对一个陌生人说：“打扰了，我不会看表，请您看一下我的手表上显示的是几点。', '发朋友圈：“我好寂寞啊” ', '做一个大家都满意的鬼脸', '深情的吻某个物品10秒', '大喊 “燃烧吧，小宇宙～”', '摸自己胸说“唉太小了”', '把自己的微信昵称改成“我是小猪佩奇”10小时', '发朋友圈:我家小狗走丢了，用搜狗搜不到，急死我了', '微信找个异性发送:我怀孕了', '发朋友圈:我再也不尿床了', '微信找个异性发送:xxx，我爱你', '请左边玩家喝一杯饮料', '分别作出喜怒哀乐四个表情', '随便找个人闻ta腋下，然后说“味道不错啊”', '把纸巾塞到鼻孔三分钟', '随便找个不认识的异性问：能加个微信吗', '随便模仿一个明星的一句话或一个动作', '自罚一杯啤酒', '你觉得自己什么时候身体发育成熟的？', '你会期待一夜情吗？', '你喜欢裸睡么？', '爱爱最喜欢哪种姿势', '对陌生人美眉挤眉弄眼', '打微信电话给你聊天列表里的第一个异性', '把微信签名改成“我是猪”24小时', '随便找个人自干三杯酒', '微信里随便找个人，发：我恨你', '说出自己的三个缺点', '最喜欢异性身体的哪个部分（不包括脖子以上部分）？', '说出内裤颜色', '说出三种避孕方式', '选一个男生 一边捶他的胸一边说： 你好讨厌哦', '对喝交杯酒,有异性的话异性优先', '有异性的话，背一位异性绕场一周', '有异性的话，与一位异性十指相扣，对视10秒', '有异性的话，喂异性吃个东西'],
    nowcontent: '点击下一题，准备开始吧！',
  },


  // 下一题
  _gonext: function() {
    let data = this.data.data;
    var result = [];
    var ranNum = 1;
    if(data.length==1){
      let gameindex = this.data.gameindex;
      if (gameindex == 0) {
        data = this.data.data0;
      } else if (gameindex == 1) {
        data = this.data.data1;
      } else if (gameindex == 2) {
        data = this.data.data0.concat(this.data.data1);
      }
      this.setData({
        data: data
      });
    }
    for (let i = 0; i < ranNum; i++) {
      let ran = Math.floor(Math.random() * data.length);
      result.push(data.splice(ran, 1)[0]);
    };
    this.setData({
      nowcontent: result[0]
    });

  },

  // 歌曲播放上一首
  _previous: function(){
    let index = this.data.musicindex;
    if(index!=0){
      let i = index - 1;
      this.setData({
        musicindex: i,
        dataJson: this.data.musicArr[i]
      });
    }
    this.audioCtx.play();
  },

  // 歌曲播放下一首
  _next: function () {
    let index = this.data.musicindex;
    if (index != 2) {
      let i = index + 1;
      this.setData({
        musicindex: i,
        dataJson: this.data.musicArr[i]
      });
    }
    this.audioCtx.play();
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    let gameindex = wx.getStorageSync('gameindex');
    let data ='';
    if (gameindex == 0) {
      data = this.data.data0;
    } else if (gameindex == 1) {
      data = this.data.data1;
    } else if (gameindex == 2) {
      data = this.data.data0.concat(this.data.data1);
    }
    this.setData({
      gameindex: gameindex,
      data: data
    });


  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    this.audioCtx = wx.createAudioContext('myAudio');
    this.audioCtx.play();
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})
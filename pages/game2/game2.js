// pages/game2/game2.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    array: ['4', '5', '6', '7', '8', '9'],
    Aarray: ['1'],
    index: 0,
    Aindex: 0,
    // 设置人数
    memberNum: 4,
    AmemberNum: 1,
    openWhiteboard: false
  },

  bindPickerChange: function (e) {
    let memberNum = this.data.array[e.detail.value];
    if (memberNum==4) {
      this.setData({
        Aarray: ['1'],
      })
    } else if (memberNum == 5 || memberNum==6) {
      this.setData({
        Aarray: ['1','2'],
      })
    } else if (memberNum > 6){
      this.setData({
        Aarray: ['1', '2', '3'],
      })
    }

    this.setData({
      index: e.detail.value,
      memberNum: memberNum
    })
    wx.setStorageSync('memberNum', this.data.memberNum);
  },


  AbindPickerChange: function(e) {
    let AmemberNum = this.data.Aarray[e.detail.value];
    this.setData({
      Aindex: e.detail.value,
      AmemberNum: AmemberNum
    })
    wx.setStorageSync('AmemberNum', this.data.AmemberNum);
  },

  switchChange: function(e) {
    this.setData({
      openWhiteboard: e.detail.value
    })
    wx.setStorageSync('openWhiteboard', e.detail.value);
  },

  _go: function() {
    wx.navigateTo({
      url: '../spy/spy'
    });
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.setStorageSync('memberNum', this.data.memberNum);
    wx.setStorageSync('AmemberNum', this.data.AmemberNum);
    wx.setStorageSync('openWhiteboard', false);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})